# This can use to operate vault as ssh CA.

> **WARNING**
> If you want to run as Production, don't use '-dev' option.
> And, need to get the config.hcl like below.
* https://github.com/hashicorp/vault/blob/master/command/server/test-fixtures/config.hcl

# Reference 
