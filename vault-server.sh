#!/bin/bash

SECRETPATH='ssh-signer'

##### vault server #####
VAULT=/usr/local/bin/vault 
$VAULT server -dev -dev-listen-address 0.0.0.0:8200 > /dev/null 2>&1 &
export VAULT_ADDR=http://127.0.0.1:8200
$VAULT secrets enable -path=$SECRETPATH ssh
$VAULT write $SECRETPATH/config/ca generate_signing_key=true

### for server

cat << EOF > server-policy.hcl
path "${SECRETPATH}/config/ca" {
  capabilities = ["read"]
}
EOF

$VAULT policy write server-policy server-policy.hcl
$VAULT token create -policy=server-policy

### for client

cat << EOF > client-policy.hcl
path "${SECRETPATH}/sign/client-role" {
    capabilities = ["create","update"]
}
EOF

cat << EOF > client-role.hcl
{
  "allow_user_certificates": true,
  "allowed_users": "*",
  "default_extensions": [
    {
      "permit-pty": "",
      "permit-agent-forwarding": ""
    }
  ],
  "key_type": "ca",
  "default_user": "root",
  "ttl": "1m0s"
}
EOF

$VAULT write $SECRETPATH/roles/client-role @./client-role.hcl
$VAULT policy write client-policy client-policy.hcl
$VAULT token create -policy=client-policy

exit 0
##### SSH Server #####

export VAULT_TOKEN="" 
$VAULT read -field=public_key ssh-signer/config/ca > /etc/ssh/trusted-ca-keys.pem
echo "TrustedUserCAKeys /etc/ssh/trusted-ca-keys.pem" >> /etc/ssh/trusted-ca-keys.pem
systemctl restart sshd

##### SSH Client #####
export VAULT_TOKEN=""
$VAULT write ssh-signer/sign/client-role public_key=@/root/.ssh/id_rsa.pub
$VAULT write -field=signed_key ssh-signer/sign/client-role public_key=@/root/.ssh/id_rsa.pub > /root/.ssh/id_rsa-cert.pub

##### verification #####
ssh -i ~/.ssh/id_rsa-cert.pub -i ~/.ssh/id_rsa root@172.16.101.101

